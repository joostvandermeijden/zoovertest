<?php

class CsvUpload extends AdminAppModel
{
	
	public $belongsTo = array('User');
	
	public $hasMany = array('WeatherForecast');
	
	public $actsAs = array('FileUpload');
	
	public $validate = array(
		'internal_name' => array(
			'rule' => array('notEmpty'),
			'required' => true,
			'message' => array('This field is required')
		),
		'filename' => array(
			'extension' => array(
				'rule' => array('extension', array('csv')),
				'message' => 'Only csv files'
			),
			'upload_file' => array(
				'rule' => array('uploadFile'),
				'message' => 'Error uploading file'
			)
		)
	);
	
}