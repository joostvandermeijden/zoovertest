<?php
Router::connect('admin*', array('plugin' => 'Admin'));
Router::connect('/Admin/:controller/:action/*', array('plugin' => 'Admin'));
Router::connect('/Admin/:controller/*', array('plugin' => 'Admin'));
Router::connect('/Admin/*', array('plugin' => 'Admin', 'controller' => 'index', 'action' => 'index'));