<table>
	<thead>
		<tr>
			<td>ID</td>
			<td>Username</td>
			<td>Created</td>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($users as $user):
			?>
			<tr>
				<td><?php echo h($user['User']['id']); ?></td>
				<td><?php echo h($user['User']['username']); ?></td>
				<td><?php echo $this->Time->niceShort($user['User']['created']); ?></td>
			</tr>
			<?php
		endforeach;
		?>
	</tbody>
</table>