<div class="users form">
<?php echo $this->Form->create('CsvUpload', array('enctype' => 'multipart/form-data')); ?>
    <fieldset>
        <legend><?php echo __('Add upload'); ?></legend>
        <?php
		echo $this->Form->input('internal_name', array('label' => 'Name'));
		echo $this->Form->input('filename', array('type' => 'file', 'label' => 'Select CSV file'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>