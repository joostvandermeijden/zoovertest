<?php

App::uses('AppController', 'Controller');

class AdminAppController extends AppController
{
	
	public $layout = 'admin';

	public $components = array(
        'Session',
		'Auth' => array(
			'loginAction' => array(
				'plugin'		=> 'Admin',
                'controller'	=> 'users',
                'action'		=> 'login'
			),
            'loginRedirect' => array(
				'plugin'		=> 'Admin',
                'controller'	=> 'index',
                'action'		=> 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'index',
                'action' => 'index'
            ),
            'authenticate' => array(
                'Form' => array(
					'passwordHasher' => 'Blowfish'
				)
            ),
			'authorize' => array('Controller')
        )
	);
	
	public function isAuthorized($user)
	{
		return (isset($user)) ? true : false;
	}
	
}