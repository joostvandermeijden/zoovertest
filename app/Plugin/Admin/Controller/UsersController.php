<?php

class UsersController extends AdminAppController
{
	
	public function index()
	{
		$this->set('users', $this->{$this->modelClass}->find('all'));
	}
	
	public function add()
	{
        if ($this->request->is('post'))
		{
            $this->User->create();
            if ($this->User->save($this->request->data))
			{
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        }
    }
	
	public function login()
	{
		if ($this->request->is('post'))
		{
			if ($this->Auth->login())
			{
				return $this->redirect($this->Auth->redirect());
			}
			$this->Session->setFlash(__('Invalid username or password, try again'));
		}
	}
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('add', 'login');
	}

}