<?php

class CsvUploadsController extends AdminAppController
{
	
	public function add()
	{
		if($this->request->is('post'))
		{
			$this->{$this->modelClass}->create();
			$newName = time().$this->request->data['CsvUpload']['filename']['name'];
			$this->request->data['CsvUpload']['file'] = $newName;
			$this->request->data['CsvUpload']['filename']['newName'] = $newName;
			$this->request->data['CsvUpload']['user_id'] = $this->Auth->user('id');
			if($this->{$this->modelClass}->save($this->request->data))
			{
				$this->__process($this->{$this->modelClass}->getLastInsertID());
				$this->Session->setFlash(__('The file has been uploaded and is processed!'));
				return $this->redirect(array('plugin' => '', 'controller' => 'WeatherForecasts', 'action' => 'index'));
			}
			$this->Session->setFlash(__('Validation errors.'));
        }
	}
	
	private function __process($id)
	{
		$this->loadModel('Station');
		$this->{$this->modelClass}->id = $id;
		$filename = $this->{$this->modelClass}->field('file');
		$lines = $this->__getLinesFromCsv($filename);
		$lines = $this->__processLines($lines, $id);
		$this->Station->saveMany($lines['stations']);
		$this->{$this->modelClass}->WeatherForecast->saveMany($lines['foreCasts']);
	}
	
	private function __getLinesFromCsv($filename)
	{
		$lines = array();
		$file = fopen(APP.DS.'files'.DS.$filename, 'r');

		$i = 0;
		while(!feof($file))
		{
			$key = ($i == 0) ? 'header' : $i;
			$lines[$key] = fgetcsv($file, 0, ';');
			$i++;
		}
		
		fclose($file);
		return $lines;
	}
	
	private function __processLines($lines, $uploadId)
	{
		$stations = array();
		$foreCasts = array();
		foreach($lines as $key => $line)
		{
			if($key != 'header')
			{
				foreach($line as $key => $val)
				{
					if($val == 'NULL')
					{
						$line[$key] = null;
					}
				}
				$stations[] = array(
					'Station' => array(
						'id' => $line[0],
						'city' => $line[1],
						'lat' => $line[2],
						'lon' => $line[3],
					)
				);
				$foreCasts[] = array(
					'WeatherForecast' => array(
						'station_id' => $line[0],
						'datetime' => $line[4],
						'max_temp' => $line[5],
						'min_temp' => $line[6],
						'chance_rain_percent' => $line[7],
						'amount_rain' => $line[8],
						'csv_upload_id' => $uploadId
					)
				);
			}
		}
		return array('stations' => $stations, 'foreCasts' => $foreCasts);
	}
	
}