<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<?php
echo $this->Form->create();

echo $this->Form->select('city', $cities, array(
	'empty' => 'Select a city'
));

echo $this->Form->select('date', $dates, array(
	'empty' => 'Select a date'
));

echo $this->Form->end();
?>

<table class="weatherForecasts">
	<thead>
		<tr>
			<td>city</td>
			<td>lat</td>
			<td>lon</td>
			<td>max temp</td>
			<td>min temp</td>
			<td>kans op regen</td>
			<td>regen</td>
			<td>tijd</td>
		</tr>
	</thead>
	<tbody>
		<tr class="empty">
			<td colspan="8">No data found</td>
		</tr>
	</tbody>
</table>

<script type="text/javascript">
	$(document).ready(function() {
		$('#WeatherForecastCity, #WeatherForecastDate').change(function() {
			$('.weatherForecasts tbody tr:not(".empty")').remove();
			city = $('#WeatherForecastCity').val();
			date = $('#WeatherForecastDate').val();
			
			$.ajax({
				url: '<?php echo Router::url(array('controller' => 'WeatherForecasts', 'action' => 'getForecasts')); ?>/'+city+'/'+date,
				success: function(data) {
					rows = $.parseJSON(data);
					if(rows.length > 0)
					{
						$.each(rows, function(key, row) {
							trRow = '<tr>';
								trRow += '<td>'+row.Station.city+'</td>';
								trRow += '<td>'+row.Station.lat+'</td>';
								trRow += '<td>'+row.Station.lon+'</td>';
								trRow += '<td>'+row.WeatherForecast.max_temp+'</td>';
								trRow += '<td>'+row.WeatherForecast.min_temp+'</td>';
								trRow += '<td>'+row.WeatherForecast.chance_rain_percent+'</td>';
								trRow += '<td>'+row.WeatherForecast.amount_rain+'</td>';
								trRow += '<td>'+row.WeatherForecast.datetime+'</td>';
							trRow += '</tr>';
							$('.weatherForecasts tbody').append(trRow);
						});
						$('.weatherForecasts tbody .empty').hide();
					}
					else 
					{
						$('.weatherForecasts tbody .empty').show();
					}
				}
			});
		});
	});
</script>