<?php

class FileUploadBehavior extends ModelBehavior
{
	
	public function uploadFile($check, $file)
	{
		$uploadData = array_shift($file);
		
		if($uploadData['size'] == 0 || $uploadData['error'] !== 0)
		{
			return false;
		}

		$uploadFolder = APP . DS . 'files';
		$fileName = time() . $uploadData['name'];
		$uploadPath =  $uploadFolder . DS . $fileName;

		if(!file_exists($uploadFolder))
		{
			mkdir($uploadFolder);
		}

		if(move_uploaded_file($uploadData['tmp_name'], $uploadPath))
		{
			return true;
		}
		return false;
	}
	
}