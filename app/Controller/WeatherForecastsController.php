<?php

class WeatherForecastsController extends AppController
{
	
	public function index()
	{
		$this->set('cities', $this->{$this->modelClass}->Station->find('list', array('fields' => array('city'))));
		
		$viewDates = array();
		$dates = $this->{$this->modelClass}->find('all', array('fields' => array('datetime' => 'datetime'), 'group' => $this->modelClass.'.datetime'));
		foreach($dates as $row)
		{
			$parsedDate = date('d-m-Y', strtotime($row['WeatherForecast']['datetime']));
			$viewDates[date('Y-m-d', strtotime($row['WeatherForecast']['datetime']))] = $parsedDate;
		}
		$this->set('dates', $viewDates);
	}
	
	public function getForecasts()
	{
		$this->autoRender = false;
		if($this->request->is('ajax'))
		{
			foreach($this->params['pass'] as $param)
			{
				if(is_numeric($param))
				{
					$stationId = $param;
				}
				else
				{
					$date = $param;
				}
			}
			
			$conditions = array();
			if(isset($stationId))
			{
				$conditions[] = array('WeatherForecast.station_id' => $stationId);
			}
			if(isset($date))
			{
				$conditions[] = array('WeatherForecast.datetime' => $date);
			}
			
			return json_encode($this->{$this->modelClass}->find('all', array(
				'contain' => 'Station',
				'conditions' => $conditions
			)));
		}
	}
	
}